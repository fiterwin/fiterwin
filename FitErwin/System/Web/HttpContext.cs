﻿using Microsoft.AspNetCore.Http;

namespace System.Web
{
    internal class HttpContext
    {
        public static Microsoft.AspNetCore.Http.HttpContext Current { get; internal set; }
        public object Response { get; internal set; }
    }
}
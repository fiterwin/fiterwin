﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FitErwin.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FitErwin.Controllers
{
    public class ExportToCSVController : Controller
    {
        private IStudentRepository _context;

        public CancellationToken IOwinResponse { get; private set; }

        public ExportToCSVController(IStudentRepository context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public void ExportToCSV()
        {

            var steps = _context.ReadAllSteps();
            StringWriter sw = new StringWriter();

            sw.WriteLine("\"DataRetrived\",\"DeviceId\",\"StudentNumber\",\"StepCount\"");

            HttpContext context = System.Web.HttpContext.Current;
            Response.Clear();
            Response.Headers.Add("content-disposition", "attachment; filename=Steps.csv");
            //Response.Clear();
            //Response.AddHeader("content-disposition", "attachment; filename=Exported_Users.csv");
            Response.ContentType = "text/csv";

            foreach (var line in steps)
            {
                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\"",
                                           line.DateRetrived,
                                           line.DeviceId,
                                           line.studentnumber,
                                           line.StepCount));
            }

            Response.WriteAsync(sw.ToString());

            Response.SendFileAsync("Steps.csv");

        }
    }
}
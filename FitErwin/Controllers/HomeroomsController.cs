﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FitErwin.Models;
using FitErwin.Services;
using Microsoft.AspNetCore.Authorization;

namespace FitErwin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class HomeroomsController : Controller
    {
        private IStudentRepository _context;

        public HomeroomsController(IStudentRepository context)
        {
            _context = context;
        }

        // GET: Homerooms
        public IActionResult Index()
        {
            return View(_context.ReadAllHomerooms());
        }

        // GET: Homerooms/Details/5
        public IActionResult Details(int id)
        {
            

            var homeroom = _context.ReadHomeroom(id);
            if (homeroom == null)
            {
                return RedirectToAction("Index","Homerooms");
            }

            return View(homeroom);
        }

        // GET: Homerooms/Create
        public IActionResult Create()
        {

            return View();
        }

        // POST: Homerooms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Homeroom homeroom)
        {
            if (ModelState.IsValid)
            {
                _context.CreateHomeroom(homeroom);
                return RedirectToAction("Index","Homerooms");
            }
            return View(homeroom);
        }

        public IActionResult Edit(int id)
        {
            var homeroom = _context.ReadHomeroom(id);
            if (homeroom == null)
            {
                return RedirectToAction("Index", "Homerooms");
            }
            return View(homeroom);
        }

        [HttpPost]
        public IActionResult Edit(Homeroom homeroom)
        {
            if (ModelState.IsValid)
            {
                _context.UpdateHomeroom(homeroom.HomeroomId, homeroom);
                return RedirectToAction("Index", "Homerooms");
            }
            return View(homeroom);
        }

        

        // GET: Homerooms/Delete/5
        public IActionResult Delete(int id)
        {


            var homeroom = _context.ReadHomeroom(id);
                
            if (homeroom == null)
            {
                return RedirectToAction("Index", "Homerooms");
            }

            return View(homeroom);
        }

        // POST: Homerooms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            _context.DeleteHomeroom(id);
            return RedirectToAction("Index","Homerooms");
        }

        
    }
}

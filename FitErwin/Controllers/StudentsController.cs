﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FitErwin.Models;
using FitErwin.Services;
using Microsoft.AspNetCore.Authorization;

namespace FitErwin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class StudentsController : Controller
    {
        private IStudentRepository _context;

        public StudentsController(IStudentRepository context)
        {
            _context = context;
        }

        // GET: Students
        public IActionResult Index()
        {
            return View(_context.ReadAllStudents());
        }

        // GET: Students/Details/5
        public IActionResult Details(int id)
        {


            var student = _context.ReadStudent(id);

            if (student == null)
            {
                return RedirectToAction("Index", "Students");

            }

            return View(student);
        }

        // GET: Students/Create
        public IActionResult Create()
        {
            ViewData["Homerooms"] = new SelectList(_context.ReadAllHomerooms(), "HomeroomName", "HomeroomName");

            return View();
        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create( Student student)
        {
            if (ModelState.IsValid)
            {
                _context.CreateStudent(student);
                return RedirectToAction("Index","Students");
            }
            return View(student);
        }

        // GET: Students/Edit/5
        public IActionResult Edit(int id)
        {

            ViewData["Homerooms"] = new SelectList(_context.ReadAllHomerooms(), "HomeroomName", "HomeroomName");

            var student = _context.ReadStudent(id);

            if (student == null)
            {
                return RedirectToAction("Index","Students");
            }
            return View(student);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, Student student)
        {

            if (ModelState.IsValid)
            {
                
                    _context.UpdateStudent(id,student);
                    return RedirectToAction("Index", "Students");
                
               
            }
            return View(student);
        }

        // GET: Students/Delete/5
        public IActionResult Delete(int id)
        {
            

            var student = _context.ReadStudent(id);

            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
             _context.DeleteStudent(id);

            
            return RedirectToAction("Index","Students");
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitErwin.Models;
using FitErwin.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FitErwin.Controllers
{
    public class HomeroomChallengesController : Controller
    {
        

        private IStudentRepository _context;

        public HomeroomChallengesController(IStudentRepository context)
        {
            _context = context;
        }

        // GET:
        public IActionResult Index()
        {
            return View(_context.ReadAllHomeroomChallenges());
        }

        // GET:
        public IActionResult Details(int id)
        {


            var homeroomchallenge = _context.ReadHomeroomChallenge(id);
            if (homeroomchallenge == null)
            {
                return RedirectToAction("Index", "HomeroomChallenges");
            }

            return View(homeroomchallenge);
        }

        // GET:
        public IActionResult Create()
        {
            ViewData["Homerooms"] = new SelectList(_context.ReadAllHomerooms(), "HomeroomId", "HomeroomName");
            ViewData["Goals"] = new SelectList(_context.ReadAllChallenges(), "Id", "Id");
            
            return View();
        }

        // POST:
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(HomeroomChallenges homeroomchallenge)
        {
            if (ModelState.IsValid)
            {
                _context.CreateHomeroomChallenge(homeroomchallenge);
                return RedirectToAction("Index", "HomeroomChallenges");
            }
            return View(homeroomchallenge);
        }

        public IActionResult Delete(int id)
        {


            var homeroomchallenge = _context.ReadHomeroomChallenge(id);

            if (homeroomchallenge == null)
            {
                return RedirectToAction("Index", "HomeroomChallenges");
            }

            return View(homeroomchallenge);
        }

        // POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            _context.DeleteHomeroomChallenge(id);
            return RedirectToAction("Index", "HomeroomChallenges");
        }


        //public IActionResult Edit(int id)
        //{
        //    var homeroomchallenge = _context.ReadHomeroomChallenge(id);

        //    if (homeroomchallenge == null)
        //    {
        //        ViewData["Homerooms"] = new SelectList(_context.ReadAllHomerooms(), "HomeroomId", "HomeroomName");
        //        ViewData["Goals"] = new SelectList(_context.ReadAllChallenges(), "Id", "Id");
        //        return RedirectToAction("Index", "HomeroomChallenges");
        //    }
        //    return View(homeroomchallenge);
        //}

        //[HttpPost]
        //public IActionResult Edit(HomeroomChallenges homeroomchallenge)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.UpdateHomeroomChallenge(homeroomchallenge.Id, homeroomchallenge);
        //        return RedirectToAction("Index", "HomeroomChallenges");
        //    }
        //    return View(homeroomchallenge);
        //}

    }
}
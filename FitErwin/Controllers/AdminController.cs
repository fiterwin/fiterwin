﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitErwin.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FitErwin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private IStudentRepository _context;

        public AdminController(IStudentRepository context)
        {
            _context = context;
        }


            public IActionResult Index()
        {
            return View();
        }

        public IActionResult CreateStudent()
        {
            return RedirectToAction("Create","Students");
        }

        public IActionResult CreateClassRoom()
        {
            return RedirectToAction("Create", "Homeroom");
        }

        public IActionResult Data()
        {
            return View();
        }

    }
}
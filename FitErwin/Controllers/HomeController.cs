﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FitErwin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;
using FitErwin.Services;
using System.Threading;

namespace FitErwin.Controllers
{
    [Authorize(Roles ="Admin")]
    public class HomeController : Controller
    {
        private IStudentRepository _context;

        public CancellationToken IOwinResponse { get; private set; }

        public HomeController(IStudentRepository context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        //public void ExportToCSV()
        //{

        //    var steps = _context.ReadAllSteps();
        //    StringWriter sw = new StringWriter();

        //    sw.WriteLine("First Name","Last Name","Email");

        //    HttpContext context = System.Web.HttpContext.Current;
        //    context.Response.Clear();
        //    context.Response.Headers.Add("content-disposition", "attachment; filename=Steps.csv");
        //    //Response.Clear();
        //    //Response.AddHeader("content-disposition", "attachment; filename=Exported_Users.csv");
        //    Response.ContentType = "text/csv";

        //    foreach (var line in steps)
        //    {
        //        sw.WriteLine(String.Format("{0} " ,"{ 1} ","{ 2} ","{ 3} ",
        //                                   line.DateRetrived,
        //                                   line.DeviceId,
        //                                   line.studentnumber,
        //                                   line.StepCount));
        //    }

        //    context.Response.WriteAsync(sw.ToString());

        //    Response.SendFileAsync("Steps.csv");

        //}

        //public IActionResult Student()
        //{
        //	ViewData["Message"] = "Your Student page.";

        //	return View();
        //}

        public IActionResult Homeroom()
		{
			ViewData["Message"] = "Your Homeroom page.";

			return View();
		}

		public IActionResult Challenges()
		{
			ViewData["Message"] = "Your Challenges page.";

			return View();
		}

		public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

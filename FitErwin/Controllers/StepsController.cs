﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitErwin.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FitErwin.Controllers
{
    //[Produces("application/json")]
    //[Route("api/Steps")]
    public class StepsController : Controller
    {

        private IStudentRepository _context;

        public StepsController(IStudentRepository context)
        {
            _context = context;
        }

        // GET: Steps
        public IActionResult Index()
        {
            return View(_context.ReadAllSteps());
        }


        // GET: 
        public IActionResult Delete(int id)
        {
         

            var step = _context.ReadStep(id);

            if (step == null)
            {
                return NotFound();
            }

            return View(step);
        }

        // POST: 
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            _context.DeleteStep(id);


            return RedirectToAction("Index", "Steps");
        }
    }
}
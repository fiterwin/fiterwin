﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FitErwin.Models;
using FitErwin.Services;
using Microsoft.AspNetCore.Authorization;

namespace FitErwin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ChallengesController : Controller
    {
        private IStudentRepository _context;

        public ChallengesController(IStudentRepository context)
        {
            _context = context;
        }

        // GET: Homerooms
        public IActionResult Index()
        {
            return View(_context.ReadAllChallenges());
        }

        // GET: Homerooms/Details/5
        public IActionResult Details(int id)
        {


            var challenge = _context.ReadChallenge(id);
            if (challenge == null)
            {
                return RedirectToAction("Index","Challenges");
            }

            return View(challenge);
        }

        // GET: Homerooms/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Homerooms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Challenges challenge)
        {
            if (ModelState.IsValid)
            {
                _context.CreateChallenge(challenge);
                return RedirectToAction("Index", "Challenges");
            }
            return View(challenge);
        }

        public IActionResult Delete(int id)
        {


            var challenge = _context.ReadChallenge(id);

            if (challenge == null)
            {
                return RedirectToAction("Index", "Challenges");
            }

            return View(challenge);
        }

        // POST: Homerooms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            _context.DeleteChallenge(id);
            return RedirectToAction("Index", "Challenges");
        }


        public IActionResult Edit(int id)
        {
            var challenge = _context.ReadChallenge(id);
            if (challenge == null)
            {
                return RedirectToAction("Index", "Challenges");
            }
            return View(challenge);
        }

        [HttpPost]
        public IActionResult Edit(Challenges challenge)
        {
            if (ModelState.IsValid)
            {
                _context.UpdateChallenge(challenge.Id, challenge);
                return RedirectToAction("Index", "Challenges");
            }
            return View(challenge);
        }
        
    }
}

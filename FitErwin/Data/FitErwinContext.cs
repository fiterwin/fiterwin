﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FitErwin.Models;

namespace FitErwin.Models
{
    public class FitErwinContext : DbContext
    {
        public FitErwinContext (DbContextOptions<FitErwinContext> options)
            : base(options)
        {
        }

        public DbSet<FitErwin.Models.Student> Student { get; set; }

        public DbSet<FitErwin.Models.Homeroom> Homeroom { get; set; }

        public DbSet<FitErwin.Models.Challenges> Challenges { get; set; }

        public DbSet<FitErwin.Models.HomeroomChallenges> HomeroomChallenges { get; set; }

        public DbSet<FitErwin.Models.Steps> Steps { get; set; }


        //public DbSet<Student> Students { get; set; }
        //public DbSet<Homeroom> Homeroom { get; set; }
        //public DbSet<Challenges> Challenges { get; set; }
    }
}

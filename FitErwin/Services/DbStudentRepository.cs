﻿using FitErwin.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitErwin.Services
{
    public class DbStudentRepository : IStudentRepository
    {
        private FitErwinContext _db;

        public DbStudentRepository(FitErwinContext db)
        {
            _db = db;
        }

        public Student CreateStudent(Student student)
        {
            _db.Student.Add(student);
            _db.SaveChanges();
            return student;
        }

        public void DeleteStudent(int studentId)
        {
            Student student = _db.Student.Find(studentId);
            _db.Student.Remove(student);
            _db.SaveChanges();
        }

        public ICollection<Student> ReadAllStudents()
        {
            return _db.Student.ToList();
        }

        public Student ReadStudent(int studentId)
        {
            return _db.Student.FirstOrDefault(s => s.Id == studentId);
        }


        /*^^^^^^^^Above is all the repo stuf for students^^^^^^^ */


        public Homeroom CreateHomeroom(Homeroom homeroom)
        {
            _db.Homeroom.Add(homeroom);
            _db.SaveChanges();
            return homeroom;
        }

        public void DeleteHomeroom(int homeroomId)
        {
            Homeroom homeroom = _db.Homeroom.Find(homeroomId);
            _db.Homeroom.Remove(homeroom);
            _db.SaveChanges();
        }


        public ICollection<Homeroom> ReadAllHomerooms()
        {
            return _db.Homeroom.ToList();
        }

        public Homeroom ReadHomeroom(int homeroomId)
        {
            return _db.Homeroom.FirstOrDefault(h => h.HomeroomId == homeroomId);
        }

        public void UpdateStudent(int studentID, Student student)
        {
            _db.Entry(student).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public Challenges CreateChallenge(Challenges challenge)
        {
            _db.Challenges.Add(challenge);
            _db.SaveChanges();
            return challenge;
        }

        public void DeleteChallenge(int challengeID)
        {
            Challenges challenge = _db.Challenges.Find(challengeID);
            _db.Challenges.Remove(challenge);
            _db.SaveChanges();
        }

        public Challenges ReadChallenge(int challengeId)
        {
            return _db.Challenges.FirstOrDefault(c => c.Id == challengeId);
        }

        public ICollection<Challenges> ReadAllChallenges()
        {
            return _db.Challenges.ToList();
        }


        /*^^^^^^^Above is repo stuff for the homeroom^^^^^^^^^*/




       

        public void UpdateHomeroom(int homeroomId, Homeroom homeroom)
        {
            _db.Entry(homeroom).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void UpdateChallenge(int challengeId, Challenges challenge)
        {
            _db.Entry(challenge).State = EntityState.Modified;
            _db.SaveChanges();
        }





        public HomeroomChallenges CreateHomeroomChallenge(HomeroomChallenges homeroomchallenge)
        {
            _db.HomeroomChallenges.Add(homeroomchallenge);
            _db.SaveChanges();
            return homeroomchallenge;
        }

        public ICollection<HomeroomChallenges> ReadAllHomeroomChallenges()
        {
            return _db.HomeroomChallenges.ToList();
        }

        public HomeroomChallenges ReadHomeroomChallenge(int Id)
        {
            return _db.HomeroomChallenges.FirstOrDefault(h => h.Id == Id);
        }

        public void DeleteHomeroomChallenge(int Id)
        {
            HomeroomChallenges homeroomchallenge = _db.HomeroomChallenges.Find(Id);
            _db.HomeroomChallenges.Remove(homeroomchallenge);
            _db.SaveChanges();
        }

        public void UpdateHomeroomChallenge(int Id, HomeroomChallenges homeroomchallenge)
        {
            _db.Entry(homeroomchallenge).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public ICollection<Steps> ReadAllSteps()
        {
            return _db.Steps.ToList();
        }

        public Steps ReadStep(int Id)
        {
            return _db.Steps.FirstOrDefault(h => h.Id == Id);
        }

        public void DeleteStep(int Id)
        {
            Steps step = _db.Steps.Find(Id);
            _db.Steps.Remove(step);
            _db.SaveChanges();
        }

    }
}

﻿using FitErwin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitErwin.Services
{
    public interface IStudentRepository
    {
        Student CreateStudent(Student student);
        Student ReadStudent(int studentId);
        void UpdateStudent(int studentID, Student student);
        void DeleteStudent(int studentID);
        ICollection<Student> ReadAllStudents();

        Homeroom CreateHomeroom(Homeroom homeroom);
        void DeleteHomeroom(int homeroomID);

        Homeroom ReadHomeroom(int HomeroomId);

        ICollection<Homeroom> ReadAllHomerooms();


        Challenges CreateChallenge(Challenges challenge);
        void DeleteChallenge(int challengeID);

        Challenges ReadChallenge(int challengeId);

        ICollection<Challenges> ReadAllChallenges();

        void UpdateHomeroom(int homeroomId, Homeroom homeroom);

        void UpdateChallenge(int challengeId, Challenges challenge);


        HomeroomChallenges CreateHomeroomChallenge(HomeroomChallenges homeroomchallenge);
        ICollection<HomeroomChallenges> ReadAllHomeroomChallenges();
        HomeroomChallenges ReadHomeroomChallenge(int Id);
        void DeleteHomeroomChallenge(int Id);

        void UpdateHomeroomChallenge(int Id, HomeroomChallenges homeromchallenge);


        ICollection<Steps> ReadAllSteps();
        Steps ReadStep(int Id);
        void DeleteStep(int Id);



    }
}

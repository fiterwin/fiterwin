﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FitErwin.Migrations
{
    public partial class mig1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Challenges",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChallengeType = table.Column<int>(nullable: false),
                    EndDate = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    StartDate = table.Column<string>(nullable: true),
                    StepGoal = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Challenges", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Homeroom",
                columns: table => new
                {
                    HomeroomId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    HomeroomName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Homeroom", x => x.HomeroomId);
                });

            migrationBuilder.CreateTable(
                name: "Steps",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateRetrived = table.Column<DateTime>(nullable: false),
                    DeviceId = table.Column<string>(nullable: true),
                    StepCount = table.Column<int>(nullable: false),
                    studentnumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Steps", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HomeroomChallenges",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    goalID = table.Column<int>(nullable: false),
                    homeroomId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HomeroomChallenges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HomeroomChallenges_Challenges_goalID",
                        column: x => x.goalID,
                        principalTable: "Challenges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HomeroomChallenges_Homeroom_homeroomId",
                        column: x => x.homeroomId,
                        principalTable: "Homeroom",
                        principalColumn: "HomeroomId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Gender = table.Column<string>(nullable: true),
                    GoalPoints = table.Column<int>(nullable: false),
                    HomeroomId = table.Column<int>(nullable: true),
                    HomeroomName = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Student_Homeroom_HomeroomId",
                        column: x => x.HomeroomId,
                        principalTable: "Homeroom",
                        principalColumn: "HomeroomId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HomeroomChallenges_goalID",
                table: "HomeroomChallenges",
                column: "goalID");

            migrationBuilder.CreateIndex(
                name: "IX_HomeroomChallenges_homeroomId",
                table: "HomeroomChallenges",
                column: "homeroomId");

            migrationBuilder.CreateIndex(
                name: "IX_Student_HomeroomId",
                table: "Student",
                column: "HomeroomId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HomeroomChallenges");

            migrationBuilder.DropTable(
                name: "Steps");

            migrationBuilder.DropTable(
                name: "Student");

            migrationBuilder.DropTable(
                name: "Challenges");

            migrationBuilder.DropTable(
                name: "Homeroom");
        }
    }
}

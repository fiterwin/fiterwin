﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitErwin.Models
{
    public class HomeroomChallenges
    {
        public int Id { get; set; }

        public int homeroomId { get; set; }
        public Homeroom homeroom { get; set; }

        

        public int goalID { get; set; }
        public Challenges goal { get; set; }

    }

}

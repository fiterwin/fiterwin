﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace FitErwin.Models
{
	public enum ChallengeType
	{
		Existence, Relatedness, Growth
	}

	public class Challenges
	{
		public int Id { get; set; }
		public ChallengeType ChallengeType { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StepGoal { get; set; }
        public string Message { set; get; }
	}
}

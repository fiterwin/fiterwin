﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitErwin.Models
{
    public class Steps
    {
        [Key]
        public int Id { get; set; }

        public DateTime DateRetrived { get; set; }
        public string DeviceId { get; set; }
        public int studentnumber { get; set; }
        public int StepCount { get; set; }
    }
}

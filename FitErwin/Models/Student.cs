﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace FitErwin.Models
{
	public class Student
	{
		public int Id { get; set; }
		public string UserName { get; set; }
		public string HomeroomName { get; set; }

        public int GoalPoints { get; set; }

        public string Gender { get; set; }

        public Homeroom Homeroom { get; set; }
	}
}
